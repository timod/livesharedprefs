package de.appsonair.sharedprefs.liveprefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import java.util.Objects;
import java.util.WeakHashMap;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

public class LiveSharedPrefs {
    private SharedPreferences prefs = null;

    private abstract class PrefLiveData<T> extends MutableLiveData<T> {
        void prefChangeDetected() {
            T value = getValue();
            if (!Objects.equals(super.getValue(), value)) {
                setValue(value);
            }
        }
    }

    private final WeakHashMap<String, PrefLiveData> prefMap = new WeakHashMap<>();
    private final String sharedPrefsName;
    private final int mode;

    private final SharedPreferences.OnSharedPreferenceChangeListener listener = (sharedPreferences, key) -> {
        PrefLiveData liveData = prefMap.get(key);
        if (liveData != null) {
            liveData.prefChangeDetected();
            Log.d("Pref", "change detected for key:" + key);
        }
    };

    public LiveSharedPrefs(@NonNull Context context, @NonNull String sharedPrefsName, int mode) {
        this(sharedPrefsName, mode);
        init(context);
    }

    public LiveSharedPrefs(@NonNull final String sharedPrefsName, int mode) {
        this.sharedPrefsName = sharedPrefsName;
        this.mode = mode;
    }

    public void init(@NonNull Context context) {
        this.prefs = context.getSharedPreferences(sharedPrefsName, mode);
        prefs.registerOnSharedPreferenceChangeListener(listener);
    }

    public MutableLiveData<Integer> createIntPref(final String key, int defaultValue) {
        PrefLiveData<Integer> pref = new PrefLiveData<Integer>() {
            @Override
            public Integer getValue() {
                return prefs.getInt(key, defaultValue);
            }
            @Override
            public void setValue(Integer value) {
                super.setValue(value);
                prefs.edit().putInt(key, value).apply();
            }
        };
        prefMap.put(key, pref);
        return pref;
    }

    public MutableLiveData<String> createStringPref(final String key, final String defaultValue) {
        PrefLiveData<String> pref = new PrefLiveData<String>() {
            @Override
            public String getValue() {
                return prefs.getString(key, defaultValue);
            }
            @Override
            public void setValue(String value) {
                super.setValue(value);
                prefs.edit().putString(key, value).apply();
            }
        };
        prefMap.put(key, pref);
        return pref;
    }
}
