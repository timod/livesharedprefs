package de.appsonair.sharedprefs;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class SecondActivity extends AppCompatActivity {

    private WurstDataModel dataModel;

    private TextView title;
    private TextView text2;
    private EditText edit;

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, SecondActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        dataModel = new WurstDataModel(this);

        title = findViewById(R.id.title);
        text2 = findViewById(R.id.text2);
        edit = findViewById(R.id.edit_field);

        title.setText(dataModel.prefTitel.getValue());

        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                dataModel.prefTitel.setValue(s.toString());
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        dataModel.prefTitel.observe(this, s -> {
            text2.setText(s);
            Log.d("Pref", "prefTitel change:" + s);
        });
    }
}
