package de.appsonair.sharedprefs;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.MutableLiveData;
import de.appsonair.sharedprefs.liveprefs.LiveSharedPrefs;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView title;
    private TextView text2;
    private EditText edit;

    private LiveSharedPrefs prefs = new LiveSharedPrefs("wurst", 0);
    private MutableLiveData<String> prefTitel = prefs.createStringPref("prefTitel", "default");
    private MutableLiveData<Integer> prefInt1 = prefs.createIntPref("it1", -1);

    private WurstDataModel dataModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prefs.init(this);

        dataModel = new WurstDataModel(this);

        title = findViewById(R.id.title);
        text2 = findViewById(R.id.text2);
        edit = findViewById(R.id.edit_field);

        findViewById(R.id.button1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SecondActivity.startActivity(MainActivity.this);
            }
        });

        //title.setText(prefTitel.getValue());
        title.setText(dataModel.prefTitel.getValue());

        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                //prefTitel.setValue(s.toString());
                dataModel.prefTitel.setValue(s.toString());
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        /*prefTitel.observe(this, s -> {
            text2.setText(s);
            Log.d("Pref", "prefTitel change:" + s);
        });*/
        dataModel.prefTitel.observe(this, s -> {
            text2.setText(s);
            Log.d("Pref", "prefTitel change:" + s);
        });
    }
}
