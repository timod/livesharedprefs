package de.appsonair.sharedprefs;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import de.appsonair.sharedprefs.liveprefs.LiveSharedPrefs;

/**
 * @author Timo Drick
 */
public class WurstDataModel {
    private final LiveSharedPrefs prefs = new LiveSharedPrefs("wurstData", 0);

    public final MutableLiveData<String> prefTitel = prefs.createStringPref("prefTitel", "default");
    public final MutableLiveData<Integer> prefInt1 = prefs.createIntPref("it1", -1);
    public final MutableLiveData<String> ali = prefs.createStringPref("alipref", null);

    public WurstDataModel(Context context) {
        prefs.init(context);
    }

}
